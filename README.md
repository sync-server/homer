# Homer homepage in docker-compose
[I use this VPS hosting](https://www.hostens.com/?affid=1251)

Works together with [jwilder/nginx-proxy](https://github.com/nginx-proxy/nginx-proxy)

`nginx-proxy` project [here](https://gitlab.com/sync-server/nginx-proxy)

[Homer main documentation](https://github.com/bastienwirtz/homer)

## Screenshot
![Homer](homer-screenshot.png)

## Links to projects
- [Nginx-proxy](https://gitlab.com/sync-server/nginx-proxy)
- [Nextcloud](https://gitlab.com/sync-server/nextcloud)
- [Syncthing](https://gitlab.com/sync-server/syncthing)
- [FilePizza](https://gitlab.com/sync-server/filepizza)
- [Prometheus and Grafana](https://gitlab.com/sync-server/grafana)
- [PrivateBin](https://gitlab.com/sync-server/privatebin)
- [Firefly-III](https://gist.github.com/optimistic5/ca5a4a8593dcdb7360f712d37a0cc657)

## Configuration
Example of `config.yml`

```yaml
title: "Homepage"
subtitle: "level101"
logo: "assets/favicon.png"
#icon: "fas fa-skull-crossbones"
#footer: '<p>Selfhosted suit</p>'

# Optional navbar
links:
  - name: "GitLab"
    icon: "fab fa-gitlab"
    url: "https://gitlab.com/sync-server"
  - name: "Hosting"
    icon: "fas fa-server"
    url: "https://www.hostens.com/?affid=1251"
  - name: "Wireguard"
    icon: "fas fa-dragon"
    url: "https://github.com/linuxserver/docker-wireguard"
  - name: "Tutanota"
    icon: "fas fa-envelope"
    url: "https://mail.tutanota.com"
  - name: "Icons"
    icon: "fab fa-fort-awesome"
    url: "https://fontawesome.com/icons?d=gallery"

# First level array represent a group
# Single service with an empty name if not using groups
services:
  - name: "Files"
    icon: "fa fa-cloud"
    items:
      - name: "Nextcloud"
        logo: "/assets/tools/nextcloud.png"
        subtitle: "File Sync & Share"
        url: "https://example.com"
      - name: "Syncthing sync-server"
        logo: "/assets/tools/syncthing.png"
        subtitle: "P2P File Sync"
        url: "https://sync.example.com"
      - name: "FilePizza"
        logo: "/assets/tools/filepizza.png"
        subtitle: "P2P File Share"
        url: "https://filepizza.example.com"
      - name: "Syncthing london-server"
        logo: "/assets/tools/syncthing.png"
        subtitle: "P2P File Sync"
        url: "https://syncl.example.com"
  - name: "Monitoring"
    icon: "fas fa-heartbeat"
    items:
      - name: "Grafana"
        logo: "/assets/tools/grafana.png"
        subtitle: "Dashboards for metrics"
        url: "https://grafana.example.com"
      - name: "Prometheus"
        logo: "/assets/tools/prometheus.png"
        subtitle: "Metric analytics"
        url: "https://prometheus.example.com"
  - name: "Tools"
    icon: "fas fa-tools"
    items:
      - name: "PrivateBin"
        logo: "/assets/tools/privatebin.png"
        subtitle: "Pastebin"
        url: "https://privatebin.example.com"
      - name: "FireFly III"
        logo: "/assets/tools/firefly-iii.png"
        subtitle: "Finance manager"
        url: "https://firefly.example.com"
```